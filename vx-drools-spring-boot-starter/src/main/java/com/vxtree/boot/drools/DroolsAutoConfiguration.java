package com.vxtree.boot.drools;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * DroolsAutoConfig
 *
 * @author WangChen
 * Created on 2024/1/30
 * @since 1.0
 */
@ComponentScan
@AutoConfiguration
public class DroolsAutoConfiguration {

}
