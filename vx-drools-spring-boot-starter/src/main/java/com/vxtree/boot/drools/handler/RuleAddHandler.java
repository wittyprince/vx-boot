package com.vxtree.boot.drools.handler;

import org.springframework.stereotype.Component;

/**
 * RuleAddHandler
 *
 * @author WangChen
 * Created on 2024/1/30
 * @since 1.0
 */
@Component
public class RuleAddHandler {

    public void add() {

    }

}
