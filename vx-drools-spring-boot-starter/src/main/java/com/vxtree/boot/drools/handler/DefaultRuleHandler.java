package com.vxtree.boot.drools.handler;

import com.vxtree.boot.drools.manager.DroolsManager;
import com.vxtree.boot.drools.rule.DroolsRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * DefaultRuleHandler
 *
 * @author WangChen
 * Created on 2024/1/31
 * @since 1.0
 */
@Primary
@Component
public class DefaultRuleHandler implements RuleHandler {

    static {
        System.out.println("DefaultRuleHandler...");
    }

    @Autowired
    private DroolsManager droolsManager;

    @Override
    public void handle() {

    }

    @Override
    public void add(DroolsRule rule) {
        add(List.of(rule));
    }

    @Override
    public void add(List<DroolsRule> rules) {
        droolsManager.add(rules);
    }

    @Override
    public void update(DroolsRule rule) {
        update(List.of(rule));
    }

    @Override
    public void update(List<DroolsRule> rules) {
        droolsManager.update(rules);
    }

    @Override
    public void delete(DroolsRule rule, String ruleName) {
        droolsManager.delete(rule, ruleName);
    }

}
