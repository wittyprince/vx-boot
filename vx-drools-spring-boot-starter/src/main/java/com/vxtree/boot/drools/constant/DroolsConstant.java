package com.vxtree.boot.drools.constant;

/**
 * DroolsConstant
 *
 * @author WangChen
 * Created on 2024/2/1
 * @since 1.0
 */
public interface DroolsConstant {

    String KIE_SESSION_PREFIX = "kb-session-";

    String RULES = "rules";

    String RULES_PATH = "rules";

    String RULES_SUFFIX = ".drl";

    String RULES_NAME = "rules";

    String RULES_GROUP = "rules";

    String RULES_VERSION = "1.0.0";

    String RULES_PACKAGE = "com.vxtree.boot.drools";

    String RULES_TEMPLATE = "template";

    String RULES_TEMPLATE_SUFFIX = ".ftl";

    String RULES_TEMPLATE_NAME = "template";

    String RULES_TEMPLATE_GROUP = "template";

    String RULES_TEMPLATE_VERSION = "1.0.0";

    String RULES_TEMPLATE_PACKAGE = "com.vxtree.boot.drools";

    String RULES_TEMPLATE_TEMPLATE = "template";

    String RULES_TEMPLATE_TEMPLATE_SUFFIX = ".ftl";

    String RULES_TEMPLATE_TEMPLATE_NAME = "template";

    String RULES_TEMPLATE_TEMPLATE_GROUP = "template";

    String RULES_TEMPLATE_TEMPLATE_VERSION = "1.0.0";

    String RULES_TEMPLATE_TEMPLATE_PACKAGE = "com.vxtree.boot.drools";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_SUFFIX = ".ftl";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_NAME = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_GROUP = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_VERSION = "1.0.0";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_PACKAGE = "com.vxtree.boot.drools";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_SUFFIX = ".ftl";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_NAME = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_GROUP = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_VERSION = "1.0.0";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_PACKAGE = "com.vxtree.boot.drools";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_SUFFIX = ".ftl";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_NAME = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_GROUP = "template";

    String RULES_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_TEMPLATE_VERSION = "1.0.0";

}
