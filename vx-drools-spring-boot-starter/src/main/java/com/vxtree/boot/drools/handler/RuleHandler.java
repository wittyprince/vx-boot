package com.vxtree.boot.drools.handler;

import com.vxtree.boot.drools.rule.DroolsRule;

import java.util.List;

/**
 * RuleAddHandler
 *
 * @author WangChen
 * Created on 2024/1/30
 * @since 1.0
 */
public interface RuleHandler {

    void handle();

    void add(DroolsRule rule);

    void add(List<DroolsRule> rules);

    void update(DroolsRule rule);

    void update(List<DroolsRule> rules);

    void delete(DroolsRule droolsRule, String ruleName);

}
