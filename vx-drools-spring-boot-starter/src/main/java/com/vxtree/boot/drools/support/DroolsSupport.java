package com.vxtree.boot.drools.support;

import com.vxtree.boot.drools.handler.RuleHandler;
import com.vxtree.boot.drools.manager.DroolsManager;
import com.vxtree.boot.drools.rule.DroolsRule;
import lombok.extern.slf4j.Slf4j;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieContainerImpl;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.vxtree.boot.drools.constant.DroolsConstant.KIE_SESSION_PREFIX;

/**
 * DroolsSupport
 *
 * @author WangChen
 * Created on 2024/1/30
 * @since 1.0
 */
@Slf4j
@Component
public class DroolsSupport {

    private final KieContainer kieContainer;

    private final DroolsManager droolsManager;

    public DroolsSupport(KieContainer kieContainer, DroolsManager droolsManager) {
        this.kieContainer = kieContainer;
        this.droolsManager = droolsManager;
    }

    public KieSession getKieSession(String kieBaseName) {
        return ((KieContainerImpl)kieContainer).getKieSession(KIE_SESSION_PREFIX + kieBaseName);
    }

    public KieContainer getKieContainer() {
        return kieContainer;
    }

    public void add(DroolsRule rule) {
        add(List.of(rule));
    }

    public void add(List<DroolsRule> rules) {
        droolsManager.add(rules);
    }

    public void update(DroolsRule rule) {
        update(List.of(rule));
    }

    public void update(List<DroolsRule> rules) {
        droolsManager.update(rules);
    }

    public void delete(DroolsRule rule, String ruleName) {
        droolsManager.delete(rule, ruleName);
    }

}
