package com.vxtree.boot.drools.rule;

import lombok.Data;

/**
 * DroolsRule
 *
 * @author WangChen
 * Created on 2024/1/25
 * @since 1.0
 */
@Data
public class DroolsRule {

    /**
     * 规则id
     */
    private String ruleId;

    /**
     * kbase的名字
     */
    private String kieBaseName;

    /**
     * 设置该kbase需要从那个目录下加载文件，这个是一个虚拟的目录，相对于 `src/main/resources`
     * 比如：kiePackageName=rules/rule01 那么当前规则文件写入路径为： kieFileSystem.write("src/main/resources/rules/rule01/1.drl")
     */
    private String kiePackageName;

    /**
     * 规则内容
     */
    private String ruleContent;

    public void validate() {
        if (isBlank(ruleId) || isBlank(kieBaseName) || isBlank(kiePackageName) || isBlank(ruleContent)) {
            throw new RuntimeException("参数有问题");
        }
    }

    private boolean isBlank(String str) {
        return str == null || str.isEmpty();
    }
}
