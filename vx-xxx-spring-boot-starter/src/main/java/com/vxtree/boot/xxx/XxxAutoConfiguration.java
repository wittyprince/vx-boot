package com.vxtree.boot.xxx;

import com.vxtree.boot.xxx.component.XxxComponent;
import com.vxtree.boot.xxx.config.XxxConfig;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Hello world!
 */
//@Configuration
@ComponentScan
//@ConditionalOnClass(XxxConfig.class)
@AutoConfiguration
public class XxxAutoConfiguration {

    static {
        System.out.println("XxxAutoConfiguration...");
    }

}
