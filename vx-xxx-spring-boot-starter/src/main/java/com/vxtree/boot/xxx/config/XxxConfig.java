package com.vxtree.boot.xxx.config;

import com.vxtree.boot.xxx.component.XxxComponent;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * XxxConfig
 *
 * @author WangChen
 * Created on 2024/1/31
 * @since 1.0
 */
@Configuration
public class XxxConfig {

    static {
        System.out.println("XxxConfig");
    }

//    @Bean
//    @ConditionalOnMissingBean
//    public XxxComponent xxxComponent() {
//        return new XxxComponent();
//    }
}
