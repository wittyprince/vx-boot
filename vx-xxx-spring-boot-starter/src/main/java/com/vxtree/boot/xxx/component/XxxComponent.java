package com.vxtree.boot.xxx.component;

import org.springframework.stereotype.Component;

/**
 * XxxComponent
 *
 * @author WangChen
 * Created on 2024/1/31
 * @since 1.0
 */
@Component
public class XxxComponent {

    static {
        System.out.println("XxxComponent");
    }

    public void test() {
        System.out.println("XxxComponent.test");
    }
}
